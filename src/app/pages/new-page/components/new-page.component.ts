import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';
import { CreateArticleModel } from 'src/app/shared/models/article-model';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styleUrls: ['./new-page.component.css']
})
export class NewPageComponent implements OnInit {
  form: FormGroup;
  data: CreateArticleModel;

  deafultForm: CreateArticleModel = {
    title: '',
    tresc: '',
    autor: '',
    opis: ''
  };

  @Output() saveEvent = new EventEmitter<CreateArticleModel>();
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group(this.deafultForm);
  }
  onSubmit(): void {
    this.data = this.form.value;
    this.saveEvent.emit(this.data);
  }
}
