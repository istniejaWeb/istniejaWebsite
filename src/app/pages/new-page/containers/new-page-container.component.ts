import { Component, OnInit } from '@angular/core';
import { CreateArticleModel } from 'src/app/shared/models/article-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-new-page-container',
  templateUrl: './new-page-container.component.html',
})
export class NewPageContainerComponent implements OnInit {

  constructor(private serverConnectService: ServerConnectService) { }
  ni = 100;

  ngOnInit(): void {
  }
  saveForm(article): void{
    this.serverConnectService.addArticle(article).subscribe({
      next: x =>  console.log(x),
      error: er =>  console.error(er)
    });
  }
}
