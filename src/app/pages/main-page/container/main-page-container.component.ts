import { Component, OnInit } from '@angular/core';
import { ArticleModel } from 'src/app/shared/models/article-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-main-page-container',
  templateUrl: './main-page-container.component.html'
})
export class MainPageContainerComponent implements OnInit {
  lastArticles: ArticleModel[] = [];
  constructor(private serverConnectService: ServerConnectService) { }

  ngOnInit(): void {
    this.serverConnectService.getLastArticles().subscribe({
      next: x => this.lastArticles = x,
      error: console.error
    });
  }

}
