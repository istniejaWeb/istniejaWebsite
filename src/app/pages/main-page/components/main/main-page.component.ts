import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ArticleModel } from 'src/app/shared/models/article-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  last: string[];
  @Input() lastArticles: ArticleModel[] = [];
  constructor() { }

  ngOnInit(): void {

  }

}
