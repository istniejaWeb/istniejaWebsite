import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ArticleModel } from 'src/app/shared/models/article-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.css']
})
export class ArticlePageComponent implements OnInit {
  @Input() article: ArticleModel;
  constructor() { }

  ngOnInit(): void {}
}
