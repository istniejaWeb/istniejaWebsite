import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ArticleModel } from 'src/app/shared/models/article-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-article-page-container',
  templateUrl: './article-page-container.component.html',
  styleUrls: ['./article-page-container.component.css']
})
export class ArticlePageContainerComponent implements OnInit {
  key: string;
  article: ArticleModel;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private pp: ServerConnectService
              ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.key = params.get('id');
      this.pp.getArticle(this.key).subscribe({
        next: x => this.article = x,
        error: er => console.error(er)
      });
    });
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
  });
  }

}

