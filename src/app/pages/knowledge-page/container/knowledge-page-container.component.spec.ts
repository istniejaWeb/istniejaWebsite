import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KnowledgePageContainerComponent } from './knowledge-page-container.component';

describe('KnowledgePageContainerComponent', () => {
  let component: KnowledgePageContainerComponent;
  let fixture: ComponentFixture<KnowledgePageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KnowledgePageContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KnowledgePageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
