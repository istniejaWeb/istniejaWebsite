import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-page-container',
  templateUrl: './contact-page-container.component.html',
  styleUrls: ['./contact-page-container.component.css']
})
export class ContactPageContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
