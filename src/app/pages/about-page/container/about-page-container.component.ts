import { Component, OnInit } from '@angular/core';
import { MemberModel } from 'src/app/shared/models/member-model';
import { ServerConnectService } from 'src/app/shared/services/server-connect.service';

@Component({
  selector: 'app-about-page-container',
  templateUrl: './about-page-container.component.html'
})
export class AboutPageContainerComponent implements OnInit {
  members: MemberModel[];
  constructor(private serverConnectService: ServerConnectService) { }

  ngOnInit(): void {
    this.serverConnectService.getMembers().subscribe({
      next: x => {this.members = x; console.log('is going', x);} // konwersja nie wymagana
    });
  }

}
