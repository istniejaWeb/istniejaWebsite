import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageContainerComponent } from './pages/main-page/container/main-page-container.component';
import { NewPageContainerComponent } from './pages/new-page/containers/new-page-container.component';
import { AboutPageContainerComponent } from './pages/about-page/container/about-page-container.component';
import { ContactPageContainerComponent } from './pages/contact-page/container/contact-page-container.component';
import { KnowledgePageContainerComponent } from './pages/knowledge-page/container/knowledge-page-container.component';
import { ArticlePageContainerComponent } from './pages/article-page/container/article-page-container.component';

const routes: Routes = [
  {path: '', component: MainPageContainerComponent},
  {path: 'about', component: AboutPageContainerComponent},
  {path: 'contact', component: ContactPageContainerComponent},
  {path: 'knowledge', component: KnowledgePageContainerComponent},
  {path: 'article/:id', component: ArticlePageContainerComponent},
  {path: 'new', component: NewPageContainerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
