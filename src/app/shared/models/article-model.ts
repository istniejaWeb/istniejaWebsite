export interface CreateArticleModel {
  title: string;
  opis: string;
  tresc: string;
  autor: string;
}
export interface ArticleModel {
  tytul: string;
  opis: string;
  tresc: string;
  autor: string;
  data: string;
  key: string;
}