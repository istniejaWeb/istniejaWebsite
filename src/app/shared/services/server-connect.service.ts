import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import {ArticleModel, CreateArticleModel } from 'src/app/shared/models/article-model';
import { tap } from 'rxjs/operators';
import { MemberDtoModel } from 'src/app/DTO/member-dto-model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ServerConnectService {
  private updateReaming =  (60 * 60 * 7);
  updateArticles = 0;
  updateMember = 0;
  constructor(private http: HttpClient) { }

  lasts$: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(null);
  members$: BehaviorSubject<MemberDtoModel[]> = new BehaviorSubject<MemberDtoModel[]>(null);
  update = 0;

  lastArticles$ = new BehaviorSubject<ArticleModel[]>(null);
  getLasts(): Observable<string[]>{
    console.log(+Date.now());
    if (this.update < +Date.now() - this.updateReaming){
      this.refreshLasts().subscribe();
    }
    this.update = +Date.now();
    return this.lasts$.asObservable();
  }

  refreshLasts(): Observable<string[]>{
    return this.http.get<string[]>('/api/last').pipe(
      tap(response => {
        console.log(response);
        this.lasts$.next(response);
      })
    );
  }
  refreshLastArticles(): void{
    const lastArticles: ArticleModel[] = [];
    this.getLasts().subscribe({
      next: result => {
        if (result != null) {
        result.forEach( name => {
          this.getArticle(name).subscribe({
            next: art => {
              art.key = name;
              lastArticles.push(art);
            },
            error: console.error
          });
        });
        }
        else{
          console.log('result is null, last is null');
        }
      },
      error: console.error,
    });
    return this.lastArticles$.next(lastArticles);
  }
  getLastArticles(): Observable<ArticleModel[]>{
    if (this.updateArticles < +Date.now() - this.updateReaming){
      this.refreshLastArticles();
    }
    this.updateArticles = +Date.now();
    return this.lastArticles$.asObservable();
  }
  getArticle(name: string): Observable<any>{
    // /api/page
    return this.http.get('/api/page/' + name);
  }
  addArticle(article: CreateArticleModel): Observable<CreateArticleModel>{
    // /api/add
    console.log(article);
    return this.http.post<CreateArticleModel>('/api/add', article, httpOptions);
  }


  getMembers(): Observable<MemberDtoModel[]>{
    console.log(+Date.now(), 'goo');
    if (this.updateMember < +Date.now() - this.updateReaming){
      this.refreshMembers().subscribe();
    }
    this.updateMember = +Date.now();
    return this.members$.asObservable();
  }

  refreshMembers(): Observable<MemberDtoModel[]>{
    return this.http.get<MemberDtoModel[]>('/api/members').pipe(
      tap(response => {
        console.log(response);
        this.members$.next(response);
      })
    );
  }
}
