import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './pages/main-page/components/main/main-page.component';
import { ContactPageComponent } from './pages/contact-page/components/contact-page.component';
import { AboutPageComponent } from './pages/about-page/components/about-page.component';
import { NewArticleComponent } from './pages/main-page/components/new-article/new-article.component';
import {CardModule} from 'primeng/card';

import {ReactiveFormsModule} from '@angular/forms';
import {ColorPickerModule} from 'primeng/colorpicker';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ButtonModule} from 'primeng/button';

import { HttpClientModule } from '@angular/common/http';
import { ArticlePageComponent } from './pages/article-page/components/article-page.component';
import { KnowledgePageComponent } from './pages/knowledge-page/components/knowledge-page.component';
import { NewPageComponent } from './pages/new-page/components/new-page.component';
import { MainPageContainerComponent } from './pages/main-page/container/main-page-container.component';
import { NewPageContainerComponent } from './pages/new-page/containers/new-page-container.component';
import { AboutPageContainerComponent } from './pages/about-page/container/about-page-container.component';
import { ArticlePageContainerComponent } from './pages/article-page/container/article-page-container.component';
import { KnowledgePageContainerComponent } from './pages/knowledge-page/container/knowledge-page-container.component';
import { ContactPageContainerComponent } from './pages/contact-page/container/contact-page-container.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    ContactPageComponent,
    AboutPageComponent,
    NewArticleComponent,
    ArticlePageComponent,
    KnowledgePageComponent,
    NewPageComponent,
    MainPageContainerComponent,
    NewPageContainerComponent,
    AboutPageContainerComponent,
    ArticlePageContainerComponent,
    KnowledgePageContainerComponent,
    ContactPageContainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CardModule,
    ReactiveFormsModule,
    ColorPickerModule,
    InputTextModule,
    InputTextareaModule,
    HttpClientModule,
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
