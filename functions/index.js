const functions = require("firebase-functions");
const admin = require("firebase-admin");
const express = require('express');
const session = require('express-session');
const cookieParser = require('cookie-parser');

const app = express();

var serviceAccount = require("./serviceAccountKey.json");
//const { Placeholder } = require("@angular/compiler/src/i18n/i18n_ast");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://istnieja-default-rtdb.firebaseio.com",
    databaseAuthVariableOverride: {
        uid: "bb198c0c-3957-4e13-bc3d-6429af4ad04b"
    },
    storageBucket: ''
});

app.use(cookieParser());
app.use(session({
    secret: "something",
    cookie: {
        secure: true
    }
}));

const db = admin.database();
const articleRef = db.ref("/articles");
const aListRef = db.ref("/aList");
const usersRef = db.ref("/users");
const membersRef = db.ref("/members");



var last = [];
var list = [];
var members = [];
aListRef.orderByKey().on("value", (data) => {
        list = Object.keys(data.val());
        console.log("change: ", list);
    },
    (error) => console.error(error.code));

aListRef.orderByKey().limitToLast(4).on("value", (data) => {
        last = Object.keys(data.val());
        last.reverse();
        console.log("changeLast: ", last);
    },
    (error) => console.error(error.code));

membersRef.on("value", (data) => {
        members = Object.values(data.val());
        console.log(members);
    },
    (error) => console.error(error.code));


app.post('/api/add', async(req, res) => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var key = Date.now();

    articleRef.child(key).set({
        tytul: req.body.title,
        opis: req.body.opis,
        tresc: req.body.tresc,
        autor: req.body.autor,
        data: dd + "." + mm + "." + yyyy
    });

    aListRef.child(key).set(true);

    console.log(req.body);
    res.json({ work: true });
});

app.get('/api/page/:page', (req, res) => {
    articleRef.child(req.params.page).once(
        "value",
        (data) => {
            res.json(data.val());
        },
        (error) => {
            console.log(error.code);
            res.json({ error: error.code });
        });
    console.log("hell yeah we finished");
});

app.get('/api/page/:page/:val', (req, res) => {
    articleRef.child(req.params.page).child(req.params.val).once("value", (data) => {
            res.json(data.val());
        },
        (error) => {
            console.log(error.code);
            res.json({ error: error.code });
        });
});

app.get('/api/last', (req, res) => {
    res.json(last);
});


app.get('/api/members', (req, res) => {
    res.json(members);
})

app.post('/api/login', (req, res) => {
    usersRef.child(req.params.login).once(
            "value",
            data => {
                if (data.val()) {
                    //inne walidacje
                    req.session.user = data.val();
                    req.session.user.login = req.params.login;
                } else {
                    res.json({ error: "brak uzytkownika" });
                    return;
                }
            }

        ).then(() => {
            console.log(req.session.user)
            res.json(req.session.user);
        })
        .catch(error => console.error(error));
})

exports.app = functions.https.onRequest(app);